﻿namespace BlackJackCore.Core.Enums
{
    public enum GameStatus
    {
        InGame = 0,
        Lost = 1,
        Win = 2
    }
}
