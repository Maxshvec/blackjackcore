﻿namespace BlackJackCore.Core.Enums
{
    public enum PlayerType
    {
        Player = 0,
        Dealer = 1,
        Bot = 2,
    }
}
