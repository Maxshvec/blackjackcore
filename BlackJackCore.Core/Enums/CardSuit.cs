﻿namespace BlackJackCore.Core.Enums
{
    public enum CardSuit
    {
        Spades = 0, 
        Clubs = 1,  
        Hearts = 2, 
        Diamonds = 3,

        // For hide card
        CardStub = 4,
    }
}
