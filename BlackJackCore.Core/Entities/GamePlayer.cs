﻿using BlackJackCore.Core.Entities.Base;
using BlackJackCore.Core.Enums;
using Dapper.Contrib.Extensions;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace BlackJackCore.Core.Entities
{
    public class GamePlayer : EntityBase
    {
        public Guid PlayerId { get; set; }

        public Guid GameId { get; set; }
        
        public GameStatus Status { get; set; }

        [Computed]
        [NotMapped]
        public virtual Game Game { get; set; }
    }
}
