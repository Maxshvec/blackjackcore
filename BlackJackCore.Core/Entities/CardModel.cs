﻿using BlackJackCore.Core.Enums;

namespace BlackJackCore.Core.Entities
{
    public class CardModel
    {
        public CardRank Rank { get; set; }

        public CardSuit Suit { get; set; }
    }
}
