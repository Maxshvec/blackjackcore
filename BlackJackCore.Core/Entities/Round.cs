﻿using BlackJackCore.Core.Entities.Base;
using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace BlackJackCore.Core.Entities
{
    public class Round : EntityBase
    {
        public int Number { get; set; }

        public Guid GameId { get; set; }

        [Computed]
        [NotMapped]
        public virtual List<Distribution> Distributions { get; set; }
    }
}
