﻿using Dapper.Contrib.Extensions;
using System;

namespace BlackJackCore.Core.Entities.Base
{
    public class EntityBase
    {
        [ExplicitKey]
        public virtual Guid Id { get; set; }

        public EntityBase()
        {
            Id = Guid.NewGuid();
        }
    }
}
