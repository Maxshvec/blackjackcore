﻿using BlackJackCore.Core.Entities.Base;
using BlackJackCore.Core.Enums;

namespace BlackJackCore.Core.Entities
{
    public class Player : EntityBase
    {
        public string Name { get; set; }

        public PlayerType PlayerType { get; set; }
    }
}
