﻿using BlackJackCore.Core.Entities.Base;
using Dapper.Contrib.Extensions;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace BlackJackCore.Core.Entities
{
    public class Game : EntityBase
    {
        public int CountPlayer { get; set; }
        
        public bool IsOver { get; set; }

        [Computed]
        [NotMapped]
        public virtual List<GamePlayer> GamePlayers { get; set; }
    }
}
