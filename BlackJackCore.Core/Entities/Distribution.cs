﻿using BlackJackCore.Core.Entities.Base;
using BlackJackCore.Core.Enums;
using Dapper.Contrib.Extensions;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace BlackJackCore.Core.Entities
{
    public class Distribution : EntityBase
    {
        public Guid PlayerId { get; set; }

        public string CardRank { get; set; }

        public CardSuit CardSuit { get; set; }

        public Guid RoundId { get; set; }

        public int Score { get; set; }

        [Computed]
        [NotMapped]
        public virtual Round Round { get; set; }
    }
}
