﻿using BlackJackCore.Core.Entities;
using BlackJackCore.Core.Enums;
using BlackJackCore.Logic.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BlackJackCore.Logic.Card
{
    public class DeckService : IDeckService
    {
        protected Random _random = new Random();

        public List<CardModel> Cards { get; private set; } = new List<CardModel>();

        public DeckService()
        {
            InitializeDeckOfCards();
        }

        private void InitializeDeckOfCards()
        {
            foreach(CardSuit item in (CardSuit[])Enum.GetValues(typeof(CardSuit)))
            {
                CardModel[] cardArray = GetCardsArray(item);

                Cards.AddRange(cardArray);
            }

            SetRandomOrder();
        }

        private CardModel[] GetCardsArray(CardSuit suitOfCard)
        {
            List<CardModel> listCards = new List<CardModel>();

            foreach (CardRank rank in Enum.GetValues(typeof(CardRank)))
            {
                CardModel card = new CardModel();
                card.Rank = rank;
                card.Suit = suitOfCard;

                listCards.Add(card);
            }

            return listCards.ToArray();
        }

        //Mix deck
        public void SetRandomOrder()
        {
            int[] randomIndexes = Enumerable.Range(0, InitialNumberCards())
                                        .OrderBy(n => _random.Next(0, CardService.NumberRanks()))
                                        .ToArray();

            List<CardModel> newList = new List<CardModel>();

            foreach (int index in randomIndexes)
            {
                newList.Add(Cards[index]);
            }

            Cards = newList;
        }

        public CardModel TakeCard()
        {
            List<CardModel> notDummyCards = Cards
                .Where(c => c.Rank != CardRank.CardStub)
                .ToList();

            if (notDummyCards.Count == 0)
            {
                throw new Exception("Deck is over");
            }

            int index = _random.Next(0, notDummyCards.Count());

            CardModel selectedCard = notDummyCards[index];

            index = Cards.IndexOf(selectedCard);

            Cards[index] = CardService.GetDummyCard();

            return selectedCard;
        }
        
        private int InitialNumberCards()
        {
            int initialNumberCards = CardService.NumberRanks() * CardService.NumberSuits();

            return initialNumberCards;
        }
    }
}
