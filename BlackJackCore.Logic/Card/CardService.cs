﻿using BlackJackCore.Core.Entities;
using BlackJackCore.Core.Enums;
using System;

namespace BlackJackCore.Logic.Card
{
    public static class CardService
    {
        public static int NumberRanks()
        {
            int numberRanks = Enum.GetValues(typeof(CardRank)).Length - 1;

            return numberRanks;
        }

        public static int NumberSuits()
        {
            int numberSuits = Enum.GetValues(typeof(CardSuit)).Length - 1;

            return numberSuits;
        }
        
        public static CardModel GetDummyCard()
        {
            CardModel card = new CardModel();
            card.Rank = CardRank.CardStub;
            card.Suit = CardSuit.CardStub;
            
            return card;
        }
    }
}
