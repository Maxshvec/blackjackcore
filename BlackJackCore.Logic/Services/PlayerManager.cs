﻿using AutoMapper;
using BlackJackCore.Core.Entities;
using BlackJackCore.Core.Enums;
using BlackJackCore.Data.Interfaces.Repositories;
using BlackJackCore.Logic.Infrastucture;
using BlackJackCore.Logic.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlackJackCore.Logic.Services
{
    public class PlayerManager : IPlayerManager
    {
        private readonly IMapper _mapper;
        private IPlayerRepository _playerRepository;

        public PlayerManager(IPlayerRepository playerRepository, IMapper mapper)
        {
            _mapper = mapper;
            _playerRepository = playerRepository;
        }

        public async Task<Player> CreatePlayer(string name)
        {
            Player player = new Player();

            player.Name = name;
            player.PlayerType = (int)PlayerType.Player;

            Guid result = await _playerRepository.Add(player);

            if (result == Guid.Empty)
            {
                throw new ValidationException("Player can't be created", "");
            }

            return player;
        }

        public async Task<List<Player>> GetComputerPlayers(int amount)
        {
            List<Player> listPlayers = (await _playerRepository.GetBots(amount)).ToList();

            Player dealer = await _playerRepository.GetDealer();

            listPlayers.Add(dealer);

            return listPlayers;
        }
        
        public async Task<List<Player>> GetPlayers()
        {
            List<Player> players = (await _playerRepository.GetPlayers()).ToList();
            
            return players;
        }
        
        public async Task CreateUser(string name)
        {
            Player newPlayer = new Player();
            newPlayer.Name = name;
            newPlayer.PlayerType = PlayerType.Player;

            Guid player = await _playerRepository.Add(newPlayer);

            if (player == Guid.Empty)
            {
                throw new ValidationException("Player can't created", "");
            }
        }
        
        public GamePlayer CreateGamePlayer(Guid playerId, Game game)
        {
            var gamePlayer = new GamePlayer();
            gamePlayer.PlayerId = playerId;
            gamePlayer.GameId = game.Id;
            gamePlayer.Status = GameStatus.InGame;

            return gamePlayer;
        }
        
    }
}
