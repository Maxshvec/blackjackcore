﻿using AutoMapper;
using BlackJackCore.Core.Entities;
using BlackJackCore.Core.Enums;
using BlackJackCore.Data.Interfaces.Repositories;
using BlackJackCore.Logic.Infrastucture;
using BlackJackCore.Logic.Interfaces;
using BlackJackCore.ViewModels.ViewModels.GameController;
using BlackJackCore.ViewModels.ViewModels.GameController.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlackJackCore.Logic.Services
{
    public class GameService : IGameService
    {
        private IDeckService _deckService;

        private IPlayerManager _playerManager;
        private IPlayerRepository _playerRepository;
        private IRoundRepository _roundRepository;
        private IGamePlayerRepository _gamePlayerRepository;
        private IGameRepository _gameRepository;
        private IDistributionRepository _distributionRepository;
        private IGameRepository _gameHistoryRepository;

        private readonly IMapper _mapper;

        private readonly int _maxNumberOfCard;
        private readonly int _maxScoreForWin;
        private readonly int _dillerMaxScore;
        private readonly int _ace;

        public GameService(
            IDeckService deckService,
            IDistributionRepository distributionRepository,
            IPlayerManager playerManager,
            IPlayerRepository playerRepository,
            IRoundRepository roundRepository,
            IGamePlayerRepository gamePlayerRepository,
            IGameRepository gameRepository,
            IGameRepository gameHistory,
            IMapper mapper
            )
        {
            _deckService = deckService;
            _distributionRepository = distributionRepository;
            _playerManager = playerManager;
            _playerRepository = playerRepository;
            _roundRepository = roundRepository;
            _gamePlayerRepository = gamePlayerRepository;
            _gameRepository = gameRepository;
            _gameHistoryRepository = gameHistory;
            _mapper = mapper;

            _maxNumberOfCard = 10;
            _maxScoreForWin = 21;
            _dillerMaxScore = 17;
            _ace = 11;
        }

        public async Task<List<PlayerGetUserGameViewItem>> GetPlayers()
        {
            List<Player> players = await _playerManager.GetPlayers();

            return _mapper.Map<List<PlayerGetUserGameViewItem>>(players);
        }

        public async Task<Guid> StartGame(string name, int amount)
        {
            Game currentGame = await CreateGame(amount);

            Player player = await _playerRepository.FindPlayerByName(name);

            List<Player> players = await _playerManager.GetComputerPlayers(amount);

            players.Add(player);

            Round currentRound = await CreateRound(currentGame.Id);

            var gamePlayers = new List<GamePlayer>();
            var distributionsList = new List<Distribution>();

            foreach (Player item in players)
            {
                GamePlayer newGamePlayer = _playerManager.CreateGamePlayer(item.Id, currentGame);
                gamePlayers.Add(newGamePlayer);

                Distribution distribution = await CreateDistribution(item.Id, currentRound);
                distributionsList.Add(distribution);
            }

            int resultOfAddingPlayers = await _gamePlayerRepository.Add(gamePlayers);

            int resultOfAddingDistributions = await _distributionRepository.Add(distributionsList);

            Guid gameId = currentGame.Id;

            Game game = await _gameRepository.Get(gameId);
            game.CountPlayer = players.Count();

            bool result = await _gameRepository.Update(game);

            if (result == false)
            {
                throw new ValidationException("Game can't be updated", "");
            }

            await TakeDistribution(gameId);

            return gameId;
        }

        public async Task<ResponseTakeGameView> TakeDistribution(Guid gameId)
        {
            Round lastRound = await _roundRepository.GeLastRoundWithDistributionByGameId(gameId);

            Round currentRound = new Round();

            if (lastRound.Distributions.Any())
            {
                currentRound = await CreateRound(gameId);

                foreach (Distribution distribution in lastRound.Distributions)
                {
                    GamePlayer gamePlayer = await _gamePlayerRepository.GetByGameIdAndPlayerId(gameId, distribution.PlayerId);

                    if (gamePlayer.Status == GameStatus.InGame)
                    {
                        Distribution newDistribution = await CreateDistribution(distribution.PlayerId, currentRound);

                        currentRound.Distributions.Add(newDistribution);
                    }
                }

                int resultOfAddingDistributions = await _distributionRepository.Add(currentRound.Distributions);
            }

            ResponseTakeGameView roundViewModel = _mapper.Map<ResponseTakeGameView>(currentRound);

            foreach (var distribution in roundViewModel.Distributions)
            {
                Player player = await _playerRepository.GetPlayerById(distribution.PlayerId);

                distribution.Name = player.Name;
                distribution.Suit = Enum.GetName(typeof(CardSuit), distribution.CardSuit);

                string card = GetCard(distribution.CardRank);

                distribution.CardRank = card;

                bool userFlag = GetUserFlag(player);

                distribution.IsUser = userFlag;
            }

            bool result = await ContinueGame(gameId);

            if (result == false)
            {
                roundViewModel.GameStatus = ViewModels.ViewModels.GameController.Enum.GameStatusEnumView.Finished;
            }

            if (result == true)
            {
                roundViewModel.GameStatus = ViewModels.ViewModels.GameController.Enum.GameStatusEnumView.InProcess;
            }

            return roundViewModel;
        }

        public async Task<bool> ContinueGame(Guid gameId)
        {
            GamePlayer user = await _gamePlayerRepository.GetUserByGameId(gameId);

            Game game = await _gameRepository.Get(gameId);

            List<GamePlayer> gamePlayers = await _gamePlayerRepository.GetGamePlayersInGameByGameId(gameId);

            if (user.Status == GameStatus.InGame && game.IsOver == false && gamePlayers.Count > 1)
            {
                return true;
            }

            return false;
        }

        public async Task<List<GameHistoryGetGameHistoriesGameViewItem>> GetGameHistories()
        {
            List<Game> gameHistory = await _gameRepository.GetGameHistory();

            List<GameHistoryGetGameHistoriesGameViewItem> gameHistoryViewModels = _mapper.Map<List<GameHistoryGetGameHistoriesGameViewItem>>(gameHistory);

            foreach (GameHistoryGetGameHistoriesGameViewItem item in gameHistoryViewModels)
            {
                Round round = await _roundRepository.GetFirstRoundWithDistributionByGameId(item.Id);

                foreach (Distribution distrib in round.Distributions)
                {
                    Player result = await _playerRepository.Get(distrib.PlayerId);

                    if (result.PlayerType == PlayerType.Player)
                    {
                        item.Name = result.Name;
                        break;
                    }
                }
            }

            return gameHistoryViewModels;
        }

        public async Task<GetGameDetailsGameView> GetGameDetails(Guid gameId)
        {
            List<Round> rounds = await _roundRepository.GetGameDetails(gameId);

            List<RoundGetGameDetailsViewItem> roundsWithDetailsViewModel = _mapper.Map<List<RoundGetGameDetailsViewItem>>(rounds);

            List<GamePlayer> winners = await _gamePlayerRepository.GetWinningPlayersByGameId(gameId);

            foreach (GamePlayer gamePlayer in winners)
            {
                WinnerGetGameDetailsGameViewItem winner = new WinnerGetGameDetailsGameViewItem();
                winner.PlayerId = gamePlayer.PlayerId;

                roundsWithDetailsViewModel.LastOrDefault().Winners.Add(winner);
            }

            foreach (RoundGetGameDetailsViewItem round in roundsWithDetailsViewModel)
            {
                round.Number += 1;

                foreach (GamePlayerGetGameDetailsGameViewItem gamePlayer in round.GamePlayers)
                {
                    Player player = await _playerRepository.GetPlayerById(gamePlayer.PlayerId);

                    gamePlayer.PlayerName = player.Name;

                    string suit = Enum.GetName(typeof(CardSuit), gamePlayer.CardSuit);

                    gamePlayer.Suit = suit;

                    string card = GetCard(gamePlayer.CardRank);

                    gamePlayer.CardRank = card;
                }

                foreach (WinnerGetGameDetailsGameViewItem winner in round.Winners)
                {
                    Player player = await _playerRepository.GetPlayerById(winner.PlayerId);

                    winner.PlayerName = player.Name;
                }
            }
            Game game = await _gameRepository.Get(gameId);

            GetGameDetailsGameView gameDetails = new GetGameDetailsGameView();

            gameDetails.IsOver = game.IsOver;
            gameDetails.Rounds = roundsWithDetailsViewModel;

            return gameDetails;
        }

        public async Task<List<RoundGetGameGameViewItem>> GetGameCard(Guid gameHistoryId)
        {
            List<Round> rounds = await _roundRepository.GetWithDistributionsByGameId(gameHistoryId);

            List<RoundGetGameGameViewItem> roundViewModel = _mapper.Map<List<RoundGetGameGameViewItem>>(rounds);

            foreach (RoundGetGameGameViewItem round in roundViewModel)
            {
                foreach (DistributionGetGameGameViewItem gamePlayer in round.Distributions)
                {
                    Player player = await _playerRepository.GetPlayerById(gamePlayer.PlayerId);
                    gamePlayer.Name = player.Name;
                    gamePlayer.Suit = Enum.GetName(typeof(CardSuit), gamePlayer.CardSuit);

                    bool userFlag = GetUserFlag(player);

                    gamePlayer.IsUser = userFlag;

                    string card = GetCard(gamePlayer.CardRank);

                    gamePlayer.CardRank = card;
                }
            }

            return roundViewModel;
        }

        public string GetCard(string cardName)
        {
            int value = (int)(CardRank)Enum.Parse(typeof(CardRank), cardName);

            if (value <= _maxNumberOfCard)
            {
                string result = value.ToString();

                return result;
            }

            return cardName;
        }

        public async Task<List<GamePlayerEnoughGameViewItem>> CompleteGame(Guid gameId)
        {
            List<GamePlayer> winningPlayers = await _gamePlayerRepository.GetWinningPlayersByGameId(gameId);

            if (!winningPlayers.Any())
            {
                Round lastRound = await _roundRepository.GeLastRoundWithDistributionByGameId(gameId);

                List<Distribution> winners = lastRound.Distributions
                    .Where(q => q.Score == lastRound.Distributions.Max(z => z.Score))
                    .Where(s => s.Score <= 21)
                    .ToList();

                foreach (Distribution distribution in winners)
                {
                    GamePlayer gamePlayer = new GamePlayer();
                    gamePlayer.PlayerId = distribution.PlayerId;
                    gamePlayer.GameId = gameId;

                    winningPlayers.Add(gamePlayer);

                    GamePlayer player = await _gamePlayerRepository.GetByGameIdAndPlayerId(gameId, distribution.PlayerId);
                    player.Status = GameStatus.Win;

                    bool result = await _gamePlayerRepository.Update(player);

                    if (result == false)
                    {
                        throw new ValidationException("GamePlayer can't be created", "");
                    }
                }
            }

            Game game = await _gameRepository.Get(gameId);

            if (game.IsOver == false)
            {
                game.IsOver = true;

                bool result = await _gameRepository.Update(game);

                if (result == false)
                {
                    throw new ValidationException("Game can't be updated", "");
                }
            }

            List<GamePlayerEnoughGameViewItem> gamePlayerViewModel = _mapper.Map<List<GamePlayerEnoughGameViewItem>>(winningPlayers);

            foreach (GamePlayerEnoughGameViewItem gamePlayer in gamePlayerViewModel)
            {
                Player player = await _playerRepository.Get(gamePlayer.PlayerId);
                gamePlayer.Name = player.Name;

                List<Round> round = await _roundRepository.GetWithDistributionsByGameIdAndPlayerId(gameId, gamePlayer.PlayerId);
                Distribution result = round.LastOrDefault().Distributions.FirstOrDefault();
                gamePlayer.Score = result.Score;
            }

            return gamePlayerViewModel;
        }

        public async Task<Distribution> CreateDistribution(Guid playerId, Round currentRound)
        {
            CardModel currentCard = _deckService.TakeCard();

            int playerScore = await GetPlayerScore(currentCard.Rank.ToString(), playerId, currentRound);

            var distribution = new Distribution();
            distribution.PlayerId = playerId;
            distribution.CardRank = currentCard.Rank.ToString();
            distribution.CardSuit = currentCard.Suit;
            distribution.RoundId = currentRound.Id;
            distribution.Score = playerScore;

            await GetPlayerStatus(playerId, playerScore, currentRound);

            return distribution;
        }

        private async Task GetPlayerStatus(Guid playerId, int playerScore, Round round)
        {
            Player diller = await _playerRepository.GetDealer();

            if (diller.Id == playerId && playerScore == _maxScoreForWin)
            {
                await UpdateGamePlayerStatus(round.GameId, playerId, GameStatus.Win);

                return;
            }

            if (diller.Id == playerId && playerScore >= _dillerMaxScore)
            {
                await UpdateGamePlayerStatus(round.GameId, playerId, GameStatus.Lost);

                return;
            }

            if (playerScore == _maxScoreForWin && round.Number == 1 | playerScore == _maxScoreForWin)
            {
                await UpdateGamePlayerStatus(round.GameId, playerId, GameStatus.Win);

                return;
            }

            if (playerScore > _maxScoreForWin)
            {
                await UpdateGamePlayerStatus(round.GameId, playerId, GameStatus.Lost);

                return;
            }
        }

        private async Task UpdateGamePlayerStatus(Guid gameId, Guid playerId, GameStatus gameStatus)
        {
            GamePlayer gamePlayer = await _gamePlayerRepository.GetByGameIdAndPlayerId(gameId, playerId);
            gamePlayer.Status = gameStatus;

            bool result = await _gamePlayerRepository.Update(gamePlayer);

            if (result == false)
            {
                throw new ValidationException("GamePlayer can't be updated", "");
            }
        }

        public async Task<Game> CreateGame(int amount)
        {
            var currentGame = new Game();

            currentGame.CountPlayer = 0;
            currentGame.IsOver = false;

            Guid result = await _gameHistoryRepository.Add(currentGame);

            if (result == Guid.Empty)
            {
                throw new ValidationException("GameHistory can't be created", "");
            }

            return currentGame;
        }

        public async Task<Round> CreateRound(Guid gameId)
        {
            Round round = await _roundRepository.GetLastRoundByGameId(gameId);

            if (round == null)
            {
                round = new Round();
                round.Number = -1;
            }

            var currentRound = new Round();

            currentRound.GameId = gameId;
            currentRound.Number = round.Number + 1;
            currentRound.Distributions = new List<Distribution>();

            Guid result = await _roundRepository.Add(currentRound);

            if (result == Guid.Empty)
            {
                throw new ValidationException("Round can't be created", "");
            }

            return currentRound;
        }

        private async Task<int> GetPlayerScore(string cardName, Guid playerId, Round round)
        {
            int result = 0;

            int value = (int)(CardWeight)Enum.Parse(typeof(CardWeight), cardName);

            if (round.Number == 0)
            {
                return value;
            }

            Round lastRound = await _roundRepository.GetPreviousRoundByGameId(round.GameId);
            Distribution distributionInLastRound = await _distributionRepository.GetDistributionByRoundIdAndPlayerId(playerId, lastRound.Id);

            if (distributionInLastRound != null)
            {
                result = distributionInLastRound.Score;
            }

            if (result >= _ace && value == _ace)
            {
                result += 1;
                return result;
            }

            int currentPlayerScore = result += value;

            return currentPlayerScore;
        }

        private bool GetUserFlag(Player player)
        {
            return player.PlayerType == (int)PlayerType.Player;
        }
    }
}
