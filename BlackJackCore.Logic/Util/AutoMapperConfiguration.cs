﻿using AutoMapper;
using BlackJackCore.Core.Entities;
using BlackJackCore.ViewModels.ViewModels.GameController;
using BlackJackCore.ViewModels.ViewModels.GameController.Response;

namespace BlackJackCore.Logic.Util
{
    public class AutoMapperConfiguration : Profile
    {
        public AutoMapperConfiguration()
        {
            CreateMap<Round, ResponseTakeGameView>().ForMember(dest => dest.Distributions, opt => opt.MapFrom(src => src.Distributions));
            CreateMap<Distribution, DistributionTakeGameViewItem>();


            CreateMap<Round, RoundGetGameGameViewItem>().ForMember(dest => dest.Distributions, opt => opt.MapFrom(src => src.Distributions));
            CreateMap<Distribution, DistributionGetGameGameViewItem>().ReverseMap();
            
            CreateMap<GamePlayer, GamePlayerGetGameDetailsGameViewItem>().ReverseMap();
            CreateMap<GamePlayer, GamePlayerEnoughGameViewItem>().ReverseMap();

            CreateMap<Player, PlayerGetUserGameViewItem>().ReverseMap();

            CreateMap<Game, GameHistoryGetGameHistoriesGameViewItem>().ReverseMap();

            CreateMap<Game, GameDetailsGameView>().ReverseMap();

            CreateMap<Round, RoundGetGameDetailsViewItem>().ForMember(dest => dest.GamePlayers, opt => opt.MapFrom(src => src.Distributions));
            CreateMap<Distribution, GamePlayerGetGameDetailsGameViewItem>();
        }
    }
}
