﻿using BlackJackCore.Core.Entities;

namespace BlackJackCore.Logic.Interfaces
{
    public interface IDeckService
    {
        CardModel TakeCard();
    }
}
