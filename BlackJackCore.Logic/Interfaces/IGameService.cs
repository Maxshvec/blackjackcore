﻿using BlackJackCore.Core.Entities;
using BlackJackCore.ViewModels.ViewModels.GameController;
using BlackJackCore.ViewModels.ViewModels.GameController.Response;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlackJackCore.Logic.Interfaces
{
    public interface IGameService
    {
        Task<Guid> StartGame(string name, int amount);

        Task<ResponseTakeGameView> TakeDistribution(Guid gameId);

        Task<List<RoundGetGameGameViewItem>> GetGameCard(Guid gameHistoryId);

        Task<List<GamePlayerEnoughGameViewItem>> CompleteGame(Guid gameId);

        Task<bool> ContinueGame(Guid gameId);

        Task<List<GameHistoryGetGameHistoriesGameViewItem>> GetGameHistories();

        Task<GetGameDetailsGameView> GetGameDetails(Guid gameId);

        Task<List<PlayerGetUserGameViewItem>> GetPlayers();
    }
}
