﻿using BlackJackCore.Core.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlackJackCore.Logic.Interfaces
{
    public interface IPlayerManager
    {
        Task<Player> CreatePlayer(string name);

        Task<List<Player>> GetComputerPlayers(int amount);

        Task<List<Player>> GetPlayers();

        Task CreateUser(string name);

        GamePlayer CreateGamePlayer(Guid playerId, Game game);
    }
}
