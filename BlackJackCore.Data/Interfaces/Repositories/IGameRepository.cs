﻿using BlackJackCore.Core.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlackJackCore.Data.Interfaces.Repositories
{
    public interface IGameRepository : IBaseRepository<Game>
    {
        Task<List<Game>> GetGameHistory();
    }
}
