﻿using BlackJackCore.Core.Entities.Base;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlackJackCore.Data.Interfaces.Repositories
{
    public interface IBaseRepository<T> where T : EntityBase
    {
        Task<Guid> Add(T entity);

        Task<bool> Delete(T entity);

        Task<IEnumerable<T>> Query(string where, object param);

        Task<T> QueryFirstOrDefault(string where, object param);

        Task<bool> Update(T entity);

        Task<T> Get(Guid id);

        Task<IEnumerable<T>> GetAll(T entity);

        Task<int> Add(List<T> entityList);
    }
}
