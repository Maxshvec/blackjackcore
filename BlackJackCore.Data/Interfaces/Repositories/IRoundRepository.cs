﻿using BlackJackCore.Core.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlackJackCore.Data.Interfaces.Repositories
{
    public interface IRoundRepository : IBaseRepository<Round>
    {
        Task<Round> GetPreviousRoundByGameId(Guid gameId);

        Task<Round> GetLastRoundByGameId(Guid gameId);

        Task<List<Round>> GetWithDistributionsByGameId(Guid gameId);

        Task<List<Round>> GetGameDetails(Guid gameId);

        Task<Round> GeLastRoundWithDistributionByGameId(Guid gameId);

        Task<Round> GetFirstRoundWithDistributionByGameId(Guid gameId);

        Task<List<Round>> GetWithDistributionsByGameIdAndPlayerId(Guid gameId, Guid playerId);
    }
}
