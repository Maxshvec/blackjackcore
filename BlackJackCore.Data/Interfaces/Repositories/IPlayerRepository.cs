﻿using BlackJackCore.Core.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlackJackCore.Data.Interfaces.Repositories
{
    public interface IPlayerRepository : IBaseRepository<Player>
    {
        Task<IEnumerable<Player>> GetBots(int amount);

        Task<Player> GetDealer();
        
        Task<IEnumerable<Player>> GetPlayers();
        
        Task<Player> FindPlayerByName(string name);

        Task<Player> GetPlayerById(Guid playerId);
    }
}
