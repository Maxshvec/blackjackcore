﻿using BlackJackCore.Core.Entities;
using System;
using System.Threading.Tasks;

namespace BlackJackCore.Data.Interfaces.Repositories
{
    public interface IDistributionRepository : IBaseRepository<Distribution>
    {
        Task<Distribution> GetDistributionByRoundIdAndPlayerId(Guid playerId, Guid roundId);
    }
}
