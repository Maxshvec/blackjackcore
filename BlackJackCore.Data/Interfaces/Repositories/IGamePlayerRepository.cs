﻿using BlackJackCore.Core.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlackJackCore.Data.Interfaces.Repositories
{
    public interface IGamePlayerRepository : IBaseRepository<GamePlayer>
    {
        Task<GamePlayer> GetUserByGameId(Guid gameId);

        Task<List<GamePlayer>> GetGamePlayersInGameByGameId(Guid gameId);

        Task<GamePlayer> GetByGameIdAndPlayerId(Guid gameId, Guid playerId);

        Task<List<GamePlayer>> GetWinningPlayersByGameId(Guid gameId);
    }
}
