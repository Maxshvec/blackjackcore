﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BlackJackCore.Data.Migrations
{
    public partial class InitializeDb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.CreateTable(
            //    name: "Distributions",
            //    columns: table => new
            //    {
            //        Id = table.Column<Guid>(nullable: false),
            //        PlayerId = table.Column<Guid>(nullable: false),
            //        CardRank = table.Column<string>(nullable: true),
            //        CardSuit = table.Column<int>(nullable: false),
            //        RoundId = table.Column<Guid>(nullable: false),
            //        Score = table.Column<int>(nullable: false)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_Distributions", x => x.Id);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "GamePlayers",
            //    columns: table => new
            //    {
            //        Id = table.Column<Guid>(nullable: false),
            //        PlayerId = table.Column<Guid>(nullable: false),
            //        GameId = table.Column<Guid>(nullable: false),
            //        Status = table.Column<int>(nullable: false)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_GamePlayers", x => x.Id);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "Games",
            //    columns: table => new
            //    {
            //        Id = table.Column<Guid>(nullable: false),
            //        CountPlayer = table.Column<int>(nullable: false),
            //        IsOver = table.Column<bool>(nullable: false)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_Games", x => x.Id);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "Players",
            //    columns: table => new
            //    {
            //        Id = table.Column<Guid>(nullable: false),
            //        Name = table.Column<string>(nullable: true),
            //        PlayerType = table.Column<int>(nullable: false)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_Players", x => x.Id);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "Rounds",
            //    columns: table => new
            //    {
            //        Id = table.Column<Guid>(nullable: false),
            //        Number = table.Column<int>(nullable: false),
            //        GameId = table.Column<Guid>(nullable: false)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_Rounds", x => x.Id);
            //    });

            //migrationBuilder.InsertData(
            //    table: "Players",
            //    columns: new[] { "Id", "Name", "PlayerType" },
            //    values: new object[,]
            //    {
            //        { new Guid("88c93660-5815-451d-992a-47d2360989ea"), "Dealer", 1 },
            //        { new Guid("8d07b51a-8899-40e4-b15d-815939a979f3"), "Bot_Vasya", 2 },
            //        { new Guid("f7a05f25-1200-46a7-936d-f3c379321bfa"), "Bot_Petya", 2 },
            //        { new Guid("747fdbe6-01f4-4db7-a49b-ea68c3b4416c"), "Bot_Kolya", 2 },
            //        { new Guid("b63e54da-ddb6-4f3f-ba3f-799a85cfab13"), "Bot_Stepan", 2 },
            //        { new Guid("1fc2f00a-c3f9-4fbb-8742-fe987b634949"), "Bot_John", 2 }
            //    });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Distributions");

            migrationBuilder.DropTable(
                name: "GamePlayers");

            migrationBuilder.DropTable(
                name: "Games");

            migrationBuilder.DropTable(
                name: "Players");

            migrationBuilder.DropTable(
                name: "Rounds");
        }
    }
}
