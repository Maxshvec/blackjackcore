﻿using BlackJackCore.Core.Entities;
using BlackJackCore.Core.Enums;
using Microsoft.EntityFrameworkCore;
using System;

namespace BlackJackCore.Data.EF
{
    public class DataContext : DbContext
    {
        public DbSet<Distribution> Distributions { get; set; }

        public DbSet<Game> Games { get; set; }

        public DbSet<GamePlayer> GamePlayers { get; set; }
        
        public DbSet<Player> Players { get; set; }
        
        public DbSet<Round> Rounds { get; set; }


        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Player>().HasData(
                new Player[]
                {
                new Player { Id= Guid.NewGuid(), Name="Dealer", PlayerType = PlayerType.Dealer },
                new Player { Id= Guid.NewGuid(), Name="Bot_Vasya", PlayerType = PlayerType.Bot },
                new Player { Id= Guid.NewGuid(), Name="Bot_Petya", PlayerType = PlayerType.Bot },
                new Player { Id= Guid.NewGuid(), Name="Bot_Kolya", PlayerType = PlayerType.Bot },
                new Player { Id= Guid.NewGuid(), Name="Bot_Stepan", PlayerType = PlayerType.Bot },
                new Player { Id= Guid.NewGuid(), Name="Bot_John", PlayerType = PlayerType.Bot},
                });

            base.OnModelCreating(modelBuilder);
        }
    }
}