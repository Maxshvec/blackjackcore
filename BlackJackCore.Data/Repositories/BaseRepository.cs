﻿using BlackJackCore.Core.Entities.Base;
using BlackJackCore.Data.Interfaces.Repositories;
using Dapper;
using Dapper.Contrib.Extensions;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace BlackJackCore.Data.Repositories
{
    public class BaseRepository<T> : IBaseRepository<T> where T : EntityBase
    {
        protected readonly string _connectionString;
        protected string _passedTableName;
        
        public BaseRepository(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("DefaultConnection");
            _passedTableName = typeof(T).Name;
            if (typeof(T).Name.EndsWith("y"))
            {
                _passedTableName = _passedTableName.Remove(_passedTableName.Length - 1) + "ies";
            }
            if (!typeof(T).Name.EndsWith("y"))
            {
                _passedTableName = _passedTableName + "s";
            }
            if (typeof(T).Name.EndsWith("ch"))
            {
                _passedTableName = _passedTableName.Remove(_passedTableName.Length - 1) + "es";
            }
        }

        public async Task<IEnumerable<T>> GetAll(T entity)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                IEnumerable<T> result = await db.GetAllAsync<T>();

                return result;
            }
        }

        public async Task<T> Get(Guid id)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                T result = await db.GetAsync<T>(id);

                return result;
            }
        }

        public async Task<Guid> Add(T entity)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                int identity = await db.InsertAsync(entity);

                Guid result = entity.Id;

                return result;
            };
        }

        public async Task<int> Add(List<T> entityList)
        {
            using(IDbConnection db = new SqlConnection(_connectionString))
            {
                int identity = await db.InsertAsync(entityList);

                return identity;
            }
        }

        public async Task<bool> Delete(T entity)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                bool result = await db.DeleteAsync(entity);

                return result;
            }
        }

        public async Task<IEnumerable<T>> Query(string where, object param)
        {
            string query = $@"SELECT * FROM { _passedTableName } { where }";

            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                IEnumerable<T> result = await db.QueryAsync<T>(query, param: param);

                return result;
            }
        }

        public async Task<T> QueryFirstOrDefault(string where, object param)
        {
            string query = $@"SELECT * FROM { _passedTableName } { where }";

            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                T result = await db.QueryFirstOrDefaultAsync<T>(query, param: param);

                return result;
            }
        }

        public async Task<bool> Update(T entity)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                bool result = await db.UpdateAsync(entity);

                return result;
            }
        }
    }
}
