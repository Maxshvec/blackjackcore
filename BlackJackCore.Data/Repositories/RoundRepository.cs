﻿using BlackJackCore.Data.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Data;
using Dapper;
using System.Linq;
using BlackJackCore.Core.Entities;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;

namespace BlackJackCore.Data.Repositories
{
    public class RoundRepository : BaseRepository<Round>, IRoundRepository
    {
        private string _selectMaxRound = "SELECT MAX(Number) FROM Rounds WHERE GameId = @gameId";

        public RoundRepository(IConfiguration configuration) : base(configuration) { }

        public async Task<Round> GeLastRoundWithDistributionByGameId(Guid gameId)
        {
            string query = $@"select * from Rounds 
		                    JOIN Distributions ON Distributions.RoundId = Rounds.Id
                            WHERE Rounds.GameId = @gameId and Rounds.Number = 
			                ({ _selectMaxRound })";

            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var roundDictionary = new Dictionary<Guid, Round>();

                IEnumerable<Round> rounds = await db.QueryAsync<Round, Distribution, Round>(
                query,
                (round, distribution) =>
                {
                    if (!roundDictionary.TryGetValue(round.Id, out Round roundEntry))
                    {
                        roundEntry = round;
                        roundEntry.Distributions = new List<Distribution>();
                        roundDictionary.Add(roundEntry.Id, roundEntry);
                    }

                    roundEntry.Distributions.Add(distribution);
                    return roundEntry;
                },
                param: new { gameId },
                splitOn: "Id");

                List<Round> roundsList = rounds
                    .Distinct()
                    .ToList();

                return roundsList.FirstOrDefault();
            }
        }

        public async Task<Round> GetPreviousRoundByGameId(Guid gameId)
        {
            string where = $@"WHERE GameId = @gameId AND Number = 
                            ({ _selectMaxRound }) - 1";

            Round round = await QueryFirstOrDefault(where, param: new { gameId });

            return round;
        }

        public async Task<Round> GetLastRoundByGameId(Guid gameId)
        {
            string where = $@"WHERE GameId = @gameId AND Number = 
                            ({ _selectMaxRound })";

            Round round = await QueryFirstOrDefault(where, param: new { gameId });

            return round;
        }

        public async Task<Round> GetFirstRoundWithDistributionByGameId(Guid gameId)
        {
            int firstRound = 0;
            
            string query = @"select * from Rounds
		                    JOIN Distributions ON Distributions.RoundId = Rounds.Id
                            WHERE Rounds.GameId = @gameId and Rounds.Number = @firstRound";

            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var roundDictionary = new Dictionary<Guid, Round>();

                IEnumerable<Round> rounds = await db.QueryAsync<Round, Distribution, Round>(
                query,
                (round, distribution) =>
                {
                    if (!roundDictionary.TryGetValue(round.Id, out Round roundEntry))
                    {
                        roundEntry = round;
                        roundEntry.Distributions = new List<Distribution>();
                        roundDictionary.Add(roundEntry.Id, roundEntry);
                    }

                    roundEntry.Distributions.Add(distribution);
                    return roundEntry;
                },
                param: new { gameId, firstRound },
                splitOn: "Id");

                List<Round> roundsList = rounds
                    .Distinct()
                    .ToList();

                return roundsList.FirstOrDefault();
            }
        }

        public async Task<List<Round>> GetWithDistributionsByGameId(Guid gameId)
        {
            string query = @"SELECT * FROM Rounds 
                            JOIN Distributions ON Distributions.RoundId = Rounds.Id 
                            WHERE Rounds.GameId = @gameId";

            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var roundDictionary = new Dictionary<Guid, Round>();

                IEnumerable<Round> list = await db.QueryAsync<Round, Distribution, Round>(
                    query,
                    (round, distribution) =>
                    {
                        if (!roundDictionary.TryGetValue(round.Id, out Round roundEntry))
                        {
                            roundEntry = round;
                            roundEntry.Distributions = new List<Distribution>();
                            roundDictionary.Add(roundEntry.Id, roundEntry);
                        }

                        roundEntry.Distributions.Add(distribution);
                        return roundEntry;
                    },
                    param: new { gameId },
                    splitOn: "Id");

                List<Round> roundsList = list
                    .Distinct()
                    .OrderBy(s => s.Number)
                    .ToList();

                return roundsList;
            }
        }

        public async Task<List<Round>> GetWithDistributionsByGameIdAndPlayerId(Guid gameId, Guid playerId)
        {
            string query = @"SELECT * FROM Rounds 
                            JOIN Distributions ON Distributions.RoundId = Rounds.Id AND PlayerId = @playerId
                            WHERE Rounds.GameId = @gameId ORDER BY Number";

            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var roundDictionary = new Dictionary<Guid, Round>();

                IEnumerable<Round> list = await db.QueryAsync<Round, Distribution, Round>(
                    query,
                    (round, distribution) =>
                    {
                        if (!roundDictionary.TryGetValue(round.Id, out Round roundEntry))
                        {
                            roundEntry = round;
                            roundEntry.Distributions = new List<Distribution>();
                            roundDictionary.Add(roundEntry.Id, roundEntry);
                        }

                        roundEntry.Distributions.Add(distribution);
                        return roundEntry;
                    },
                    param: new { gameId, playerId },
                    splitOn: "Id");

                List<Round> roundsList = list
                    .Distinct()
                    .OrderBy(s => s.Number)
                    .ToList();

                return roundsList;
            }
        }

        public async Task<List<Round>> GetGameDetails(Guid gameId)
        {
            string query = @"SELECT Distinct * FROM Rounds 
                           JOIN Distributions ON Distributions.RoundId = Rounds.Id
                            WHERE Rounds.GameId = @gameId
                            ORDER BY Number";

            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var roundDictionary = new Dictionary<Guid, Round>();

                IEnumerable<Round> list = await db.QueryAsync<Round, Distribution, Round>(
                    query,
                    (round, distribution) =>
                    {
                        if (!roundDictionary.TryGetValue(round.Id, out Round roundEntry))
                        {
                            roundEntry = round;
                            roundEntry.Distributions = new List<Distribution>();

                            roundDictionary.Add(roundEntry.Id, roundEntry);
                        }
                        roundEntry.Distributions.Add(distribution);

                        return roundEntry;
                    },
                    param: new { gameId },
                    splitOn: "Id");

                List<Round> result = list
                    .Distinct()
                    .ToList();

                return result;
            }
        }
    }
}
