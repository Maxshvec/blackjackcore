﻿using BlackJackCore.Core.Entities;
using BlackJackCore.Data.Interfaces.Repositories;
using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace BlackJackCore.Data.Repositories
{
    public class GameRepository : BaseRepository<Game>, IGameRepository
    {
        public GameRepository(IConfiguration configuration) : base(configuration) { }

        public async Task<List<Game>> GetGameHistory()
        {
            string query = @"SELECT * FROM Games
                            JOIN GamePlayers ON GamePlayers.GameId = Games.Id";

            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var gameHistoryDictionary = new Dictionary<Guid, Game>();

                IEnumerable<Game> gameHistories = await db.QueryAsync<Game, GamePlayer, Game>(
                    query,
                    (game, gamePlayer) =>
                    {
                        if (!gameHistoryDictionary.TryGetValue(game.Id, out Game gameEntry))
                        {
                            gameEntry = game;
                            gameEntry.GamePlayers = new List<GamePlayer>();
                            gameHistoryDictionary.Add(gameEntry.Id, gameEntry);
                        }

                        gameEntry.GamePlayers.Add(gamePlayer);

                        return gameEntry;
                    },
                    splitOn: "Id");

                List<Game> gameList = gameHistories
                    .Distinct()
                    .ToList();

                return gameList;
            }
        }
    }
}
