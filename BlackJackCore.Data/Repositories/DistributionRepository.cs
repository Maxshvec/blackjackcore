﻿using BlackJackCore.Core.Entities;
using BlackJackCore.Data.Interfaces.Repositories;
using Microsoft.Extensions.Configuration;
using System;
using System.Threading.Tasks;

namespace BlackJackCore.Data.Repositories
{
    public class DistributionRepository : BaseRepository<Distribution>, IDistributionRepository
    {
        public DistributionRepository(IConfiguration configuration) : base(configuration) { }

        public async Task<Distribution> GetDistributionByRoundIdAndPlayerId(Guid playerId, Guid roundId)
        {
            string where = @"WHERE PlayerId = @playerId AND RoundId = @roundId";

            Distribution distribution = await QueryFirstOrDefault(where, param: new { playerId, roundId });

            return distribution;
        }
    }
}
