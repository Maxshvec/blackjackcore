﻿using BlackJackCore.Core.Entities;
using BlackJackCore.Core.Enums;
using BlackJackCore.Data.Interfaces.Repositories;
using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace BlackJackCore.Data.Repositories
{
    public class PlayerRepository : BaseRepository<Player>, IPlayerRepository
    {
        public PlayerRepository(IConfiguration configuration) : base(configuration)
        {
        }

        public async Task<IEnumerable<Player>> GetBots(int amount)
        {
            var status = PlayerType.Bot;
            
            string query = $@"SELECT TOP { amount } * FROM { _passedTableName } WHERE PlayerType = @status";

            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                IEnumerable<Player> result = await db.QueryAsync<Player>(query, param: new { status });

                return result;
            }
        }

        public async Task<Player> GetDealer()
        {
            PlayerType status = PlayerType.Dealer;

            string where = @"WHERE PlayerType = @status";

            Player player = await QueryFirstOrDefault(where, param: new { status });

            return player;
        }
        
        public async Task<IEnumerable<Player>> GetPlayers()
        {
            PlayerType status = PlayerType.Player;

            string where = @"WHERE PlayerType = @status";

            IEnumerable<Player> players = await Query(where, param: new { status });

            return players;
        }

        public async Task<Player> FindPlayerByName(string name)
        {
            PlayerType status = PlayerType.Player;

            string where = @"WHERE PlayerType = @status AND Name = @name";

            Player player = await QueryFirstOrDefault(where, param: new { status, name });

            return player;
        }

        public async Task<Player> GetPlayerById(Guid playerId)
        {
            string where = @"WHERE Id = @playerId";

            Player player = await QueryFirstOrDefault(where, param: new { playerId });

            return player;
        }
    }
}
