﻿using BlackJackCore.Core.Entities;
using BlackJackCore.Core.Enums;
using BlackJackCore.Data.Interfaces.Repositories;
using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace BlackJackCore.Data.Repositories
{
    public class GamePlayerRepository : BaseRepository<GamePlayer>, IGamePlayerRepository
    {
        public GamePlayerRepository(IConfiguration configuration) : base(configuration) { }

        public async Task<GamePlayer> GetUserByGameId(Guid gameId)
        {
            PlayerType playerType = PlayerType.Player;

            string query = @"SELECT GamePlayers.* from GamePlayers
                            JOIN Players ON Players.Id = GamePlayers.PlayerId AND Players.PlayerType = @playerType
                            WHERE GameId = @gameId ";

            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                GamePlayer gamePlayer = await db.QueryFirstOrDefaultAsync<GamePlayer>(query, param: new { playerType, gameId });

                return gamePlayer;
            }
        }

        public async Task<List<GamePlayer>> GetGamePlayersInGameByGameId(Guid gameId)
        {
            GameStatus gameStatus = GameStatus.InGame;

            string where = @"WHERE GameId = @gameId AND Status = @gameStatus";

            List<GamePlayer> gamePlayers = (await Query(where, param: new { gameId, gameStatus })).ToList();
            
            return gamePlayers;
        }

        public async Task<GamePlayer> GetByGameIdAndPlayerId(Guid gameId, Guid playerId)
        {
            string where = @"WHERE GameId = @gameId AND PlayerId = @playerId";

            GamePlayer gamePlayer = await QueryFirstOrDefault(where, param: new { gameId, playerId });

            return gamePlayer;
        }

        public async Task<List<GamePlayer>> GetWinningPlayersByGameId(Guid gameId)
        {
            GameStatus gameStatus = GameStatus.Win;

            string where = @"WHERE GameId = @gameId AND Status = @gameStatus";

            List<GamePlayer> winningPlayers = (await Query(where, param: new { gameId, gameStatus })).ToList();
            
            return winningPlayers;
        }
    }
}
