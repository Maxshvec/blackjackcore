﻿using System;

namespace BlackJackCore.ViewModels.ViewModels.GameController
{
    public class GameDetailsGameView
    {
        public Guid Id { get; set; }

        public bool IsOver { get; set; }
    }
}
