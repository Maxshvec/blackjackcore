﻿using System;
using System.Collections.Generic;

namespace BlackJackCore.ViewModels.ViewModels.GameController
{
    public class GetHistoryGameView
    {
        public List<GameHistoryGetGameHistoriesGameViewItem> GameHistories { get; set; }
    }

    public class GameHistoryGetGameHistoriesGameViewItem
    {
        public Guid Id { get; set; }

        public int CountPlayer { get; set; }

        public string Name { get; set; }
    }
}
