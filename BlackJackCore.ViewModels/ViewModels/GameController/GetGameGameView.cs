﻿using BlackJackCore.ViewModels.ViewModels.GameController.Enum;
using System;
using System.Collections.Generic;

namespace BlackJackCore.ViewModels.ViewModels.GameController
{
    public class GetGameGameView
    {
        public List<RoundGetGameGameViewItem> Rounds { get; set; }
    }

    public class RoundGetGameGameViewItem
    {
        public int Number { get; set; }

        public GameStatusEnumView GameStatus { get; set; }

        public List<DistributionGetGameGameViewItem> Distributions { get; set; }
    }

    public class DistributionGetGameGameViewItem
    {
        public Guid PlayerId { get; set; }

        public string Name { get; set; }

        public bool IsUser { get; set; }

        public string CardRank { get; set; }

        public int CardSuit { get; set; }

        public string Suit { get; set; }

        public int Score { get; set; }
    }
}
