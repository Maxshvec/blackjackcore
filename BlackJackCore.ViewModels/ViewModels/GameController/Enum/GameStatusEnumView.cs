﻿namespace BlackJackCore.ViewModels.ViewModels.GameController.Enum
{
    public enum GameStatusEnumView
    {
        InProcess = 0,
        Finished = 1
    }
}
