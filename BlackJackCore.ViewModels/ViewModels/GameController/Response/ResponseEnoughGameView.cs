﻿using System;
using System.Collections.Generic;

namespace BlackJackCore.ViewModels.ViewModels.GameController.Response
{
    public class ResponseEnoughGameView
    {
        public List<GamePlayerEnoughGameViewItem> GamePlayers { get; set; }
    }

    public class GamePlayerEnoughGameViewItem
    {
        public Guid PlayerId { get; set; }

        public string Name { get; set; }
        
        public int Score { get; set; }
    }
}
