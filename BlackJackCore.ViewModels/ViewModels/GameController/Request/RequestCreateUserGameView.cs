﻿namespace BlackJackCore.ViewModels.ViewModels.GameController.Request
{
    public class RequestCreateUserGameView
    {
        public string Name { get; set; }
    }
}
