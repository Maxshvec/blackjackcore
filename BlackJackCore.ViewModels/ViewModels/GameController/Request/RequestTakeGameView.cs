﻿namespace BlackJackCore.ViewModels.ViewModels.GameController.Request
{
    public class RequestTakeGameView
    {
        public string Id { get; set; }
    }
}
