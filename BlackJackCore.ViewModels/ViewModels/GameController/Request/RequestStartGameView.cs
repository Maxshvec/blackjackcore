﻿namespace BlackJackCore.ViewModels.ViewModels.GameController.Request
{
    public class RequestStartGameView
    {
        public string Name { get; set; }

        public int NumberBots { get; set; }
    }
}
