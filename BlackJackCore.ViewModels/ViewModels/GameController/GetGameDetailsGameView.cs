﻿using System;
using System.Collections.Generic;

namespace BlackJackCore.ViewModels.ViewModels.GameController
{
    public class GetGameDetailsGameView
    {
        public bool IsOver { get; set; }

        public List<RoundGetGameDetailsViewItem> Rounds { get; set; }
    }

    public class RoundGetGameDetailsViewItem
    {
        public int Number { get; set; }

        public Guid GameId { get; set; }

        public List<GamePlayerGetGameDetailsGameViewItem> GamePlayers { get; set; }

        public List<WinnerGetGameDetailsGameViewItem> Winners { get; set; } = new List<WinnerGetGameDetailsGameViewItem>();
    }

    public class GamePlayerGetGameDetailsGameViewItem
    {
        public Guid PlayerId { get; set; }

        public string PlayerName { get; set; }
        
        public string CardRank { get; set; }

        public int CardSuit { get; set; }

        public string Suit { get; set; }

        public string Score { get; set; }

        public int GameStatus { get; set; }
    }

    public class WinnerGetGameDetailsGameViewItem
    {
        public Guid PlayerId { get; set; }

        public string PlayerName { get; set; }
    }
}
