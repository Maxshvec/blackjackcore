(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["history-history-module"],{

/***/ "./src/app/history/history-list/history-list.component.html":
/*!******************************************************************!*\
  !*** ./src/app/history/history-list/history-list.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<br>\r\n\r\n<div *ngIf=\"done\">\r\n    <div class=\"row\">\r\n        <div class=\"col-md-6 col-md-offset-3\">\r\n            <kendo-grid [data]=\"gameHistory.gameHistories\" [height]=\"410\" [resizable]=\"true\">\r\n                <kendo-grid-column field=\"countPlayer\" title=\"Players in game\" width=\"40\">\r\n                </kendo-grid-column>\r\n                <kendo-grid-column field=\"name\" title=\"Name\" width=\"250\">\r\n                </kendo-grid-column>\r\n                <kendo-grid-column field=\"id\" title=\"Continue\" width=\"250\">\r\n                    <ng-template kendoGridCellTemplate let-dataItem>\r\n                        <!-- <input type=\"checkbox\" [checked]=\"dataItem.Discontinued\" disabled/> -->\r\n                        <input type=\"button\" [routerLink]=\"['/detail', dataItem.id]\" value=\"Detail\" class=\"btn btn-default\" />\r\n                    </ng-template>\r\n                </kendo-grid-column>\r\n            </kendo-grid>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./src/app/history/history-list/history-list.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/history/history-list/history-list.component.ts ***!
  \****************************************************************/
/*! exports provided: HistoryListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HistoryListComponent", function() { return HistoryListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_start_start_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/start/start.service */ "./src/app/start/start.service.ts");



var HistoryListComponent = /** @class */ (function () {
    function HistoryListComponent(htttpService) {
        this.htttpService = htttpService;
        this.done = false;
    }
    HistoryListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.htttpService.getHistory()
            .subscribe(function (response) {
            _this.gameHistory = response;
            _this.done = true;
        }, function (error) {
            console.log(error);
            _this.done = false;
        });
    };
    HistoryListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-history-list',
            template: __webpack_require__(/*! ./history-list.component.html */ "./src/app/history/history-list/history-list.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_start_start_service__WEBPACK_IMPORTED_MODULE_2__["StartService"]])
    ], HistoryListComponent);
    return HistoryListComponent;
}());



/***/ }),

/***/ "./src/app/history/history-routing.module.ts":
/*!***************************************************!*\
  !*** ./src/app/history/history-routing.module.ts ***!
  \***************************************************/
/*! exports provided: HistoryRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HistoryRoutingModule", function() { return HistoryRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _history_list_history_list_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./history-list/history-list.component */ "./src/app/history/history-list/history-list.component.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");




var routes = [
    {
        path: '',
        component: _history_list_history_list_component__WEBPACK_IMPORTED_MODULE_2__["HistoryListComponent"]
    }
];
var HistoryRoutingModule = /** @class */ (function () {
    function HistoryRoutingModule() {
    }
    HistoryRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], HistoryRoutingModule);
    return HistoryRoutingModule;
}());



/***/ }),

/***/ "./src/app/history/history.module.ts":
/*!*******************************************!*\
  !*** ./src/app/history/history.module.ts ***!
  \*******************************************/
/*! exports provided: HistoryModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HistoryModule", function() { return HistoryModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _history_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./history-routing.module */ "./src/app/history/history-routing.module.ts");
/* harmony import */ var _history_list_history_list_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./history-list/history-list.component */ "./src/app/history/history-list/history-list.component.ts");
/* harmony import */ var _progress_kendo_angular_grid__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @progress/kendo-angular-grid */ "./node_modules/@progress/kendo-angular-grid/dist/es/index.js");






var HistoryModule = /** @class */ (function () {
    function HistoryModule() {
    }
    HistoryModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _history_routing_module__WEBPACK_IMPORTED_MODULE_3__["HistoryRoutingModule"],
                _progress_kendo_angular_grid__WEBPACK_IMPORTED_MODULE_5__["GridModule"]
            ],
            declarations: [_history_list_history_list_component__WEBPACK_IMPORTED_MODULE_4__["HistoryListComponent"]]
        })
    ], HistoryModule);
    return HistoryModule;
}());



/***/ })

}]);
//# sourceMappingURL=history-history-module.js.map