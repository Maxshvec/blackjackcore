(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["detail-detail-module"],{

/***/ "./src/app/detail/detail-list/detail-list.component.html":
/*!***************************************************************!*\
  !*** ./src/app/detail/detail-list/detail-list.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<br>\r\n<div *ngIf=\"done\">\r\n    <div class=\"row\">\r\n        <div class=\"col-md-5 col-md-offset-3\">\r\n            <div *ngFor=\"let round of gameDetails.rounds\">\r\n                <div class=\"badge\">\r\n                    Round: {{round.number}}\r\n                </div>\r\n                <div *ngFor=\"let gamePlayer of round.gamePlayers\" class=\"panel-body\">\r\n                    {{gamePlayer.playerName}} |\r\n                    {{gamePlayer.rankCard}}\r\n                    {{gamePlayer.suit}}\r\n                    <div>\r\n                        Current score: {{gamePlayer.score}}\r\n                    </div>\r\n                </div>\r\n                <hr>\r\n            </div>\r\n            <!--button submit this-->\r\n        </div>\r\n\r\n        <div *ngIf=\"gameDetails.isOver\" class=\"col-md-1\">\r\n            <div class=\"badge\">\r\n                Winner:\r\n            </div>\r\n            <div *ngFor=\"let round of gameDetails.rounds\">\r\n                <ng-container *ngFor=\"let winner of round.winners\">\r\n                    {{winner.playerName}}\r\n                </ng-container>\r\n            </div>\r\n        </div>\r\n        <div *ngIf=\"gameDetails.isOver==false\" class=\"col-md-1\">\r\n            <input type=\"submit\" (click)=\"continue()\" class=\"btn btn-success\" value=\"Сontinue game\" />\r\n        </div>\r\n    </div>"

/***/ }),

/***/ "./src/app/detail/detail-list/detail-list.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/detail/detail-list/detail-list.component.ts ***!
  \*************************************************************/
/*! exports provided: DetailListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetailListComponent", function() { return DetailListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_start_start_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/start/start.service */ "./src/app/start/start.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");




var DetailListComponent = /** @class */ (function () {
    function DetailListComponent(httpService, activateRoute, router) {
        this.httpService = httpService;
        this.activateRoute = activateRoute;
        this.router = router;
        this.done = false;
        this.id = activateRoute.snapshot.params['id'];
    }
    DetailListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.httpService.getDetail(this.id)
            .subscribe(function (response) {
            _this.gameDetails = response;
            console.log(_this.gameDetails);
            _this.done = true;
        }, function (error) {
            console.log(error);
            _this.done = false;
        });
    };
    DetailListComponent.prototype.continue = function () {
        this.router.navigate(['/game', this.id]);
    };
    DetailListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-detail-list',
            template: __webpack_require__(/*! ./detail-list.component.html */ "./src/app/detail/detail-list/detail-list.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_start_start_service__WEBPACK_IMPORTED_MODULE_2__["StartService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], DetailListComponent);
    return DetailListComponent;
}());



/***/ }),

/***/ "./src/app/detail/detail-routing.module.ts":
/*!*************************************************!*\
  !*** ./src/app/detail/detail-routing.module.ts ***!
  \*************************************************/
/*! exports provided: DetailRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetailRoutingModule", function() { return DetailRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _detail_list_detail_list_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./detail-list/detail-list.component */ "./src/app/detail/detail-list/detail-list.component.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");




var routes = [
    {
        path: '',
        component: _detail_list_detail_list_component__WEBPACK_IMPORTED_MODULE_2__["DetailListComponent"]
    }
];
var DetailRoutingModule = /** @class */ (function () {
    function DetailRoutingModule() {
    }
    DetailRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], DetailRoutingModule);
    return DetailRoutingModule;
}());



/***/ }),

/***/ "./src/app/detail/detail.module.ts":
/*!*****************************************!*\
  !*** ./src/app/detail/detail.module.ts ***!
  \*****************************************/
/*! exports provided: DetailModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetailModule", function() { return DetailModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _detail_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./detail-routing.module */ "./src/app/detail/detail-routing.module.ts");
/* harmony import */ var _detail_list_detail_list_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./detail-list/detail-list.component */ "./src/app/detail/detail-list/detail-list.component.ts");
/* harmony import */ var _progress_kendo_angular_grid__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @progress/kendo-angular-grid */ "./node_modules/@progress/kendo-angular-grid/dist/es/index.js");






var DetailModule = /** @class */ (function () {
    function DetailModule() {
    }
    DetailModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _detail_routing_module__WEBPACK_IMPORTED_MODULE_3__["DetailRoutingModule"],
                _progress_kendo_angular_grid__WEBPACK_IMPORTED_MODULE_5__["GridModule"]
            ],
            declarations: [_detail_list_detail_list_component__WEBPACK_IMPORTED_MODULE_4__["DetailListComponent"]]
        })
    ], DetailModule);
    return DetailModule;
}());



/***/ })

}]);
//# sourceMappingURL=detail-detail-module.js.map