(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["game-game-module"],{

/***/ "./src/app/game/game-desk/game-desk.component.css":
/*!********************************************************!*\
  !*** ./src/app/game/game-desk/game-desk.component.css ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".vcenter {\r\n    display: inline-block;\r\n    vertical-align: middle;\r\n    float: none;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZ2FtZS9nYW1lLWRlc2svZ2FtZS1kZXNrLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxzQkFBc0I7SUFDdEIsdUJBQXVCO0lBQ3ZCLFlBQVk7Q0FDZiIsImZpbGUiOiJzcmMvYXBwL2dhbWUvZ2FtZS1kZXNrL2dhbWUtZGVzay5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnZjZW50ZXIge1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcclxuICAgIGZsb2F0OiBub25lO1xyXG59Il19 */"

/***/ }),

/***/ "./src/app/game/game-desk/game-desk.component.html":
/*!*********************************************************!*\
  !*** ./src/app/game/game-desk/game-desk.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"done\">\r\n\r\n  <div class=\"row\">\r\n    <div style=\"text-align:center\">\r\n      <button (click)=\"enough()\" class=\"btn btn-danger\">Enough</button>\r\n    </div>\r\n\r\n    <div style=\"text-align:center\">\r\n      <button (click)=\"take()\" class=\"btn btn-success\">Take</button>\r\n    </div>\r\n  </div>\r\n<br>\r\n\r\n  <div class=\"row\">\r\n    <div class=\"panel panel-primary col-md-1 col-md-offset-4\">\r\n      <div *ngFor=\"let user of userScore\" class=\"panel-heading\">\r\n        {{user.playerName}}\r\n        {{user.score}}\r\n      </div>\r\n      <div *ngFor=\"let item of score\" class=\"panel-body\">\r\n        {{item.playerName}}\r\n        {{item.score}}\r\n      </div>\r\n    </div>\r\n\r\n    <div class=\"vcenter\">\r\n      <ng-container *ngFor=\"let round of listRoundViewModel.rounds\">\r\n        <div class=\"col-md-8\">\r\n          <div class=\"panel panel-success\">\r\n            <div *ngIf=\"round.number > 1\" class=\"panel-heading\">\r\n              Round: {{round.number}}\r\n            </div>\r\n            <div *ngFor=\"let player of round.distributions\" class=\"panel-body\">\r\n              {{player.name}}\r\n              {{player.cardRank}}\r\n              {{player.suit}}\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </ng-container>\r\n    </div>\r\n\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/game/game-desk/game-desk.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/game/game-desk/game-desk.component.ts ***!
  \*******************************************************/
/*! exports provided: GameDeskComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GameDeskComponent", function() { return GameDeskComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _start_start_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../start/start.service */ "./src/app/start/start.service.ts");
/* harmony import */ var _shared_model_requestTakeGameView__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../shared/model/requestTakeGameView */ "./src/app/shared/model/requestTakeGameView.ts");
/* harmony import */ var src_app_shared_model_enum_gameStatus__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/shared/model/enum/gameStatus */ "./src/app/shared/model/enum/gameStatus.ts");






var GameDeskComponent = /** @class */ (function () {
    function GameDeskComponent(router, httpService, activateRoute) {
        this.router = router;
        this.httpService = httpService;
        this.activateRoute = activateRoute;
        this.done = false;
        this.score = [];
        this.userScore = [];
        this.max = 0;
        this.id = activateRoute.snapshot.params['id'];
        this.requestTakeGameView = new _shared_model_requestTakeGameView__WEBPACK_IMPORTED_MODULE_4__["RequestTakeGameView"]();
    }
    GameDeskComponent.prototype.ngOnInit = function () {
        if (this.id) {
            this.loadGame();
        }
    };
    GameDeskComponent.prototype.loadGame = function () {
        var _this = this;
        this.httpService.getGame(this.id)
            .subscribe(function (response) {
            _this.listRoundViewModel = response;
            response.rounds.forEach(function (element) { return _this.max = element.number; });
            response.rounds.forEach(function (element) {
                if (element.number == _this.max) {
                    _this.CountScore(element);
                }
            });
            _this.done = true;
            _this.listRoundViewModel.rounds.reverse();
        }, function (error) {
            console.log(error);
            _this.done = false;
        });
    };
    GameDeskComponent.prototype.take = function () {
        var _this = this;
        this.requestTakeGameView.id = this.id;
        this.httpService.take(this.requestTakeGameView)
            .subscribe(function (response) {
            if (response.gameStatus == src_app_shared_model_enum_gameStatus__WEBPACK_IMPORTED_MODULE_5__["GameStatus"].InProcess) {
                _this.listRoundViewModel.rounds.unshift(response);
                _this.CountScore(response);
            }
            if (response.gameStatus == src_app_shared_model_enum_gameStatus__WEBPACK_IMPORTED_MODULE_5__["GameStatus"].Finished) {
                _this.enough();
            }
        }, function (error) {
            console.log(error);
        });
    };
    GameDeskComponent.prototype.CountScore = function (element) {
        var _this = this;
        this.score = [];
        this.userScore = [];
        element.distributions.forEach(function (item) {
            if (item.isUser == true) {
                _this.userScore.push({ playerName: item.name, score: item.score });
            }
            else {
                _this.score.push({ playerName: item.name, score: item.score });
            }
        });
    };
    GameDeskComponent.prototype.enough = function () {
        this.router.navigate(['/enough', this.id]);
    };
    GameDeskComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-game-desk',
            template: __webpack_require__(/*! ./game-desk.component.html */ "./src/app/game/game-desk/game-desk.component.html"),
            styles: [__webpack_require__(/*! ./game-desk.component.css */ "./src/app/game/game-desk/game-desk.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _start_start_service__WEBPACK_IMPORTED_MODULE_3__["StartService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])
    ], GameDeskComponent);
    return GameDeskComponent;
}());



/***/ }),

/***/ "./src/app/game/game-routing.module.ts":
/*!*********************************************!*\
  !*** ./src/app/game/game-routing.module.ts ***!
  \*********************************************/
/*! exports provided: GameRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GameRoutingModule", function() { return GameRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _game_desk_game_desk_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./game-desk/game-desk.component */ "./src/app/game/game-desk/game-desk.component.ts");




var routes = [
    {
        path: '',
        component: _game_desk_game_desk_component__WEBPACK_IMPORTED_MODULE_3__["GameDeskComponent"]
    }
];
var GameRoutingModule = /** @class */ (function () {
    function GameRoutingModule() {
    }
    GameRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], GameRoutingModule);
    return GameRoutingModule;
}());



/***/ }),

/***/ "./src/app/game/game.module.ts":
/*!*************************************!*\
  !*** ./src/app/game/game.module.ts ***!
  \*************************************/
/*! exports provided: GameModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GameModule", function() { return GameModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _game_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./game-routing.module */ "./src/app/game/game-routing.module.ts");
/* harmony import */ var _game_desk_game_desk_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./game-desk/game-desk.component */ "./src/app/game/game-desk/game-desk.component.ts");





var GameModule = /** @class */ (function () {
    function GameModule() {
    }
    GameModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _game_routing_module__WEBPACK_IMPORTED_MODULE_3__["GameRoutingModule"]
            ],
            declarations: [_game_desk_game_desk_component__WEBPACK_IMPORTED_MODULE_4__["GameDeskComponent"]]
        })
    ], GameModule);
    return GameModule;
}());



/***/ }),

/***/ "./src/app/shared/model/enum/gameStatus.ts":
/*!*************************************************!*\
  !*** ./src/app/shared/model/enum/gameStatus.ts ***!
  \*************************************************/
/*! exports provided: GameStatus, GameStat */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GameStatus", function() { return GameStatus; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GameStat", function() { return GameStat; });
var GameStatus;
(function (GameStatus) {
    GameStatus[GameStatus["InProcess"] = 0] = "InProcess";
    GameStatus[GameStatus["Finished"] = 1] = "Finished";
})(GameStatus || (GameStatus = {}));
var GameStat = /** @class */ (function () {
    function GameStat() {
    }
    return GameStat;
}());



/***/ }),

/***/ "./src/app/shared/model/requestTakeGameView.ts":
/*!*****************************************************!*\
  !*** ./src/app/shared/model/requestTakeGameView.ts ***!
  \*****************************************************/
/*! exports provided: RequestTakeGameView */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RequestTakeGameView", function() { return RequestTakeGameView; });
var RequestTakeGameView = /** @class */ (function () {
    function RequestTakeGameView() {
    }
    return RequestTakeGameView;
}());



/***/ })

}]);
//# sourceMappingURL=game-game-module.js.map