(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["enough-enough-module"],{

/***/ "./src/app/enough/enough-list/enough-list.component.html":
/*!***************************************************************!*\
  !*** ./src/app/enough/enough-list/enough-list.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div style=\"text-align: center\">\r\n    <h2>Game is over</h2>\r\n    <div *ngIf=\"done\">\r\n        <div *ngIf=\"responseEnoughGame.gamePlayers.length==0\">\r\n            <h2>Too many points</h2>\r\n        </div>\r\n        \r\n        <table *ngIf=\"responseEnoughGame.gamePlayers.length > 0\" class=\"table table-bordered table-hover\" style=\"margin: 0px auto; table-layout: auto; width: auto;\">\r\n            <tr *ngFor=\"let player of responseEnoughGame.gamePlayers\">\r\n                <th>\r\n                    {{player.name}}\r\n                </th>\r\n                <th>\r\n                    {{player.score}}\r\n                </th>\r\n            </tr>\r\n        </table>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./src/app/enough/enough-list/enough-list.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/enough/enough-list/enough-list.component.ts ***!
  \*************************************************************/
/*! exports provided: EnoughListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EnoughListComponent", function() { return EnoughListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_start_start_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/start/start.service */ "./src/app/start/start.service.ts");
/* harmony import */ var _shared_model_requestEnoughGameView__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../shared/model/requestEnoughGameView */ "./src/app/shared/model/requestEnoughGameView.ts");





var EnoughListComponent = /** @class */ (function () {
    function EnoughListComponent(httpService, activateRoure) {
        this.httpService = httpService;
        this.activateRoure = activateRoure;
        this.done = false;
        this.id = activateRoure.snapshot.params['id'];
        this.requestEnoughGame = new _shared_model_requestEnoughGameView__WEBPACK_IMPORTED_MODULE_4__["RequestEnoughGameView"]();
    }
    EnoughListComponent.prototype.ngOnInit = function () {
        if (this.id) {
            this.loadEnough();
        }
    };
    EnoughListComponent.prototype.loadEnough = function () {
        var _this = this;
        this.requestEnoughGame.id = this.id;
        this.httpService.enough(this.requestEnoughGame)
            .subscribe(function (response) {
            _this.responseEnoughGame = response;
            _this.done = true;
        }, function (error) {
            console.log(error);
            _this.done = false;
        });
    };
    EnoughListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-enough-list',
            template: __webpack_require__(/*! ./enough-list.component.html */ "./src/app/enough/enough-list/enough-list.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_start_start_service__WEBPACK_IMPORTED_MODULE_3__["StartService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])
    ], EnoughListComponent);
    return EnoughListComponent;
}());



/***/ }),

/***/ "./src/app/enough/enough-routing.module.ts":
/*!*************************************************!*\
  !*** ./src/app/enough/enough-routing.module.ts ***!
  \*************************************************/
/*! exports provided: EnoughRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EnoughRoutingModule", function() { return EnoughRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _enough_list_enough_list_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./enough-list/enough-list.component */ "./src/app/enough/enough-list/enough-list.component.ts");




var routes = [
    {
        path: '',
        component: _enough_list_enough_list_component__WEBPACK_IMPORTED_MODULE_3__["EnoughListComponent"]
    }
];
var EnoughRoutingModule = /** @class */ (function () {
    function EnoughRoutingModule() {
    }
    EnoughRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], EnoughRoutingModule);
    return EnoughRoutingModule;
}());



/***/ }),

/***/ "./src/app/enough/enough.module.ts":
/*!*****************************************!*\
  !*** ./src/app/enough/enough.module.ts ***!
  \*****************************************/
/*! exports provided: EnoughModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EnoughModule", function() { return EnoughModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _enough_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./enough-routing.module */ "./src/app/enough/enough-routing.module.ts");
/* harmony import */ var _enough_list_enough_list_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./enough-list/enough-list.component */ "./src/app/enough/enough-list/enough-list.component.ts");





var EnoughModule = /** @class */ (function () {
    function EnoughModule() {
    }
    EnoughModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _enough_routing_module__WEBPACK_IMPORTED_MODULE_3__["EnoughRoutingModule"]
            ],
            declarations: [_enough_list_enough_list_component__WEBPACK_IMPORTED_MODULE_4__["EnoughListComponent"]]
        })
    ], EnoughModule);
    return EnoughModule;
}());



/***/ }),

/***/ "./src/app/shared/model/requestEnoughGameView.ts":
/*!*******************************************************!*\
  !*** ./src/app/shared/model/requestEnoughGameView.ts ***!
  \*******************************************************/
/*! exports provided: RequestEnoughGameView */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RequestEnoughGameView", function() { return RequestEnoughGameView; });
var RequestEnoughGameView = /** @class */ (function () {
    function RequestEnoughGameView() {
    }
    return RequestEnoughGameView;
}());



/***/ })

}]);
//# sourceMappingURL=enough-enough-module.js.map