(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./detail/detail.module": [
		"./src/app/detail/detail.module.ts",
		"detail-detail-module"
	],
	"./enough/enough.module": [
		"./src/app/enough/enough.module.ts",
		"enough-enough-module"
	],
	"./game/game.module": [
		"./src/app/game/game.module.ts",
		"game-game-module"
	],
	"./history/history.module": [
		"./src/app/history/history.module.ts",
		"history-history-module"
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids) {
		return Promise.resolve().then(function() {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}
	return __webpack_require__.e(ids[1]).then(function() {
		var id = ids[0];
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _start_start_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./start/start.component */ "./src/app/start/start.component.ts");




var routes = [
    {
        path: '', component: _start_start_component__WEBPACK_IMPORTED_MODULE_3__["StartComponent"]
    },
    // { path: 'gameDesk/:id', component: GameDeskComponent },
    {
        path: 'game/:id',
        loadChildren: './game/game.module#GameModule'
        // loadChildren: () => GameModule
    },
    {
        path: 'enough/:id',
        loadChildren: './enough/enough.module#EnoughModule'
    },
    {
        path: 'history',
        loadChildren: './history/history.module#HistoryModule'
    },
    {
        path: 'detail/:id',
        loadChildren: './detail/detail.module#DetailModule'
    }
    // { path: '**', redirectTo: '/'}
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<br>\r\n\r\n<div style=\"text-align:center\">\r\n  <h1>\r\n    <a routerLink=\"\">{{ title }}</a>\r\n  </h1>\r\n  \r\n  <div>\r\n    <button (click)=\"getHistory()\" class=\"btn btn-primary\">Game History</button>\r\n</div>\r\n</div>\r\n\r\n<router-outlet></router-outlet>"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _start_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./start.service */ "./src/app/start.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");




var AppComponent = /** @class */ (function () {
    function AppComponent(router) {
        this.router = router;
        this.title = "Black Jack";
    }
    AppComponent.prototype.getHistory = function () {
        this.router.navigate(['/history']);
    };
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            providers: [_start_service__WEBPACK_IMPORTED_MODULE_2__["StartService"]],
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _start_start_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./start/start.component */ "./src/app/start/start.component.ts");
/* harmony import */ var _progress_kendo_angular_buttons__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @progress/kendo-angular-buttons */ "./node_modules/@progress/kendo-angular-buttons/dist/es/index.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _progress_kendo_angular_inputs__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @progress/kendo-angular-inputs */ "./node_modules/@progress/kendo-angular-inputs/dist/es/index.js");
/* harmony import */ var _progress_kendo_angular_dropdowns__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @progress/kendo-angular-dropdowns */ "./node_modules/@progress/kendo-angular-dropdowns/dist/es/index.js");
/* harmony import */ var _progress_kendo_angular_grid__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @progress/kendo-angular-grid */ "./node_modules/@progress/kendo-angular-grid/dist/es/index.js");













var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"],
                _start_start_component__WEBPACK_IMPORTED_MODULE_7__["StartComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_4__["AppRoutingModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormsModule"],
                _progress_kendo_angular_buttons__WEBPACK_IMPORTED_MODULE_8__["ButtonsModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_9__["BrowserAnimationsModule"],
                _progress_kendo_angular_inputs__WEBPACK_IMPORTED_MODULE_10__["InputsModule"],
                _progress_kendo_angular_dropdowns__WEBPACK_IMPORTED_MODULE_11__["DropDownsModule"],
                _progress_kendo_angular_grid__WEBPACK_IMPORTED_MODULE_12__["GridModule"]
            ],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/shared/model/requestCreateUserGameView.ts":
/*!***********************************************************!*\
  !*** ./src/app/shared/model/requestCreateUserGameView.ts ***!
  \***********************************************************/
/*! exports provided: RequestCreateUserGameView */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RequestCreateUserGameView", function() { return RequestCreateUserGameView; });
var RequestCreateUserGameView = /** @class */ (function () {
    function RequestCreateUserGameView() {
    }
    return RequestCreateUserGameView;
}());



/***/ }),

/***/ "./src/app/shared/model/requestStartGameView.ts":
/*!******************************************************!*\
  !*** ./src/app/shared/model/requestStartGameView.ts ***!
  \******************************************************/
/*! exports provided: RequestStartGameView */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RequestStartGameView", function() { return RequestStartGameView; });
var RequestStartGameView = /** @class */ (function () {
    function RequestStartGameView() {
    }
    return RequestStartGameView;
}());



/***/ }),

/***/ "./src/app/start.service.ts":
/*!**********************************!*\
  !*** ./src/app/start.service.ts ***!
  \**********************************/
/*! exports provided: StartService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StartService", function() { return StartService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");



var StartService = /** @class */ (function () {
    function StartService(http) {
        this.http = http;
    }
    StartService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], StartService);
    return StartService;
}());



/***/ }),

/***/ "./src/app/start/start.component.css":
/*!*******************************************!*\
  !*** ./src/app/start/start.component.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3N0YXJ0L3N0YXJ0LmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/start/start.component.html":
/*!********************************************!*\
  !*** ./src/app/start/start.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<br>\r\n<br>\r\n<br>\r\n<form #myForm=\"ngForm\" novalidate style=\"text-align:center; \">\r\n  <label>Type your name</label>\r\n  <div class=\"form-group\">\r\n\r\n    <kendo-dropdownlist [data]=\"data\" [filterable]=\"true\" [textField]=\"'name'\" [valueField]=\"''\" (filterChange)=\"handleFilter($event)\"\r\n      [(ngModel)]=\"selectedName\" name=\"selectedName\" required>\r\n      <ng-template kendoDropDownListNoDataTemplate>\r\n        <div>\r\n          No data found.\r\n          <ng-container *ngIf=\"filter\">Do you want to add new item - '{{ filter }}' ?</ng-container>\r\n          <br />\r\n          <button *ngIf=\"filter\" class=\"k-button\" (click)=\"addNew()\">Add new item</button>\r\n        </div>\r\n      </ng-template>\r\n    </kendo-dropdownlist>\r\n  </div>\r\n\r\n  <label>Select numbers of bots</label>\r\n  <div class=\"form-group\">\r\n    <kendo-dropdownlist [data]=\"listBots\" [(ngModel)]=\"numberBots\" name=\"numberBots\" required>\r\n    </kendo-dropdownlist>\r\n  </div>\r\n\r\n  <div class=\"form-group\">\r\n    <input type=\"submit\" (click)=\"submit(selectedName.name, numberBots)\" [disabled]=\"myForm.invalid\" class=\"btn btn-default\"\r\n      value=\"Start game\" />\r\n  </div>\r\n</form>\r\n\r\n<router-outlet></router-outlet>"

/***/ }),

/***/ "./src/app/start/start.component.ts":
/*!******************************************!*\
  !*** ./src/app/start/start.component.ts ***!
  \******************************************/
/*! exports provided: StartComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StartComponent", function() { return StartComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _start_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./start.service */ "./src/app/start/start.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _shared_model_requestStartGameView__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../shared/model/requestStartGameView */ "./src/app/shared/model/requestStartGameView.ts");
/* harmony import */ var _shared_model_requestCreateUserGameView__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../shared/model/requestCreateUserGameView */ "./src/app/shared/model/requestCreateUserGameView.ts");






var StartComponent = /** @class */ (function () {
    function StartComponent(router, httpService) {
        this.router = router;
        this.httpService = httpService;
        this.listBots = [1, 2, 3, 4, 5];
        this.requestStartGameView = new _shared_model_requestStartGameView__WEBPACK_IMPORTED_MODULE_4__["RequestStartGameView"];
        this.requestCreateUserGameView = new _shared_model_requestCreateUserGameView__WEBPACK_IMPORTED_MODULE_5__["RequestCreateUserGameView"];
    }
    StartComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.httpService.getUsers()
            .subscribe(function (response) {
            _this.users = response;
            _this.data = response;
        }, function (error) {
            console.log(error);
        });
    };
    StartComponent.prototype.addNew = function () {
        var _this = this;
        this.requestCreateUserGameView.name = this.filter;
        this.httpService.createUser(this.requestCreateUserGameView).subscribe(function (response) {
            _this.users.push(response);
            _this.data.push(response);
            _this.handleFilter(_this.filter);
        }, function (error) {
            console.log(error);
        });
    };
    StartComponent.prototype.handleFilter = function (value) {
        this.filter = value;
        this.data = this.users.filter(function (s) { return s.name.toLowerCase().indexOf(value.toLowerCase()) !== -1; });
    };
    StartComponent.prototype.submit = function (name, numberBots) {
        var _this = this;
        this.requestStartGameView.name = name;
        this.requestStartGameView.numberBots = numberBots;
        this.httpService.startGame(this.requestStartGameView)
            .subscribe(function (response) {
            _this.id = response;
            _this.router.navigate(['/game', _this.id]);
        }, function (error) {
            console.log(error);
        });
    };
    StartComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-start',
            template: __webpack_require__(/*! ./start.component.html */ "./src/app/start/start.component.html"),
            providers: [_start_service__WEBPACK_IMPORTED_MODULE_2__["StartService"]],
            styles: [__webpack_require__(/*! ./start.component.css */ "./src/app/start/start.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _start_service__WEBPACK_IMPORTED_MODULE_2__["StartService"]])
    ], StartComponent);
    return StartComponent;
}());



/***/ }),

/***/ "./src/app/start/start.service.ts":
/*!****************************************!*\
  !*** ./src/app/start/start.service.ts ***!
  \****************************************/
/*! exports provided: StartService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StartService", function() { return StartService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");




var StartService = /** @class */ (function () {
    function StartService(http) {
        this.http = http;
        this.baseUrl = src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].baseUrl;
    }
    StartService.prototype.createUser = function (model) {
        return this.http.post(this.baseUrl + 'Game/CreateUser', model);
    };
    StartService.prototype.getUsers = function () {
        return this.http.get(this.baseUrl + 'Game/GetUsers');
    };
    StartService.prototype.startGame = function (model) {
        return this.http.post(this.baseUrl + 'Game/StartGame', model);
    };
    StartService.prototype.getGame = function (id) {
        return this.http.get(this.baseUrl + 'Game/GetGame?id=' + id);
    };
    StartService.prototype.take = function (model) {
        return this.http.post(this.baseUrl + 'Game/Take', model);
    };
    StartService.prototype.enough = function (model) {
        return this.http.post(this.baseUrl + 'Game/Enough', model);
    };
    StartService.prototype.getHistory = function () {
        return this.http.get(this.baseUrl + 'Game/GetHistory');
    };
    StartService.prototype.getDetail = function (id) {
        return this.http.get(this.baseUrl + 'Game/GetDetail?id=' + id);
    };
    StartService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], StartService);
    return StartService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false,
    baseUrl: 'https://localhost:44371/api/'
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\Anuitex\source\repos\BitBucket\BlackJack\BlackJackCore.Angular\BlackJackApp\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map