import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GetGameDetailsGameView } from 'src/app/shared/model/getGameDetailsGameView';
import { DetailService } from 'src/app/shared/services/detail.service';

@Component({
    selector: 'app-detail-list',
    templateUrl: './detail-list.component.html',
    providers: [DetailService]
})

export class DetailListComponent implements OnInit {

    id: string;
    done: boolean = false;
    gameDetails: GetGameDetailsGameView;

    constructor(private httpService: DetailService, private activateRoute: ActivatedRoute, private router: Router) {

        this.id = activateRoute.snapshot.params['id'];
    }

    ngOnInit() {
        this.httpService.getDetail(this.id)
        .subscribe(
            response => {
                this.gameDetails = response;
                console.log(this.gameDetails);
                
                this.done = true;
            }
        )
    }

    continue() {
        this.router.navigate(['/game', this.id]);
    }
}