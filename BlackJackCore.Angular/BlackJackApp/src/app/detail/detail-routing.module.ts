import { Routes, RouterModule } from "@angular/router";
import { DetailListComponent } from './detail-list/detail-list.component';
import { NgModule } from '@angular/core';

const routes: Routes = [
    {
        path: '',
        component: DetailListComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class DetailRoutingModule { }
