import { Component, OnInit } from '@angular/core'
import { GetHistoryGameView } from 'src/app/shared/model/getHistoryGameView';
import { HistoryService } from 'src/app/shared/services/history.service';


@Component({
    selector: 'app-history-list',
    templateUrl: './history-list.component.html',
    styleUrls: ['./history-list.component.css'],
    providers: [HistoryService]
})

export class HistoryListComponent implements OnInit {

    public gameHistory: GetHistoryGameView;

    public done: boolean = false;

    constructor(private htttpService: HistoryService) {

    }

    ngOnInit() {

        this.htttpService.getHistory()
            .subscribe(
                response => {
                    this.gameHistory = response;
                    this.done = true;
                })

    }
}
