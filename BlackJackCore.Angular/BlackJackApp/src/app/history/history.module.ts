import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HistoryRoutingModule } from './history-routing.module';
import { HistoryListComponent } from './history-list/history-list.component';
import { GridModule } from '@progress/kendo-angular-grid';
import { MatTabsModule } from '@angular/material/tabs';
import { MatIconModule } from '@angular/material/icon';
import {MatGridListModule} from '@angular/material/grid-list';


@NgModule({
    imports: [
        CommonModule,
        HistoryRoutingModule,
        GridModule,
        MatTabsModule,
        MatIconModule,
        MatGridListModule
    ],
    declarations: [HistoryListComponent]
})

export class HistoryModule { }
