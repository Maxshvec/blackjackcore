import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StartComponent } from './start/start.component';

const routes: Routes = [
  { 
    path: '', component: StartComponent 
  },
  {
    path: 'game/:id',
    loadChildren: './game/game.module#GameModule'
  },
  {
    path: 'enough/:id',
    loadChildren: './enough/enough.module#EnoughModule'
  },
  {
    path: 'history',
    loadChildren: './history/history.module#HistoryModule'
  },
  {
    path: 'detail/:id',
    loadChildren: './detail/detail.module#DetailModule'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
