import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RequestTakeGameView } from 'src/app/shared/model/requestTakeGameView';
import { GetGameGameView } from 'src/app/shared/model/getGameGameView';
import { GameStatus } from 'src/app/shared/model/enum/gameStatus';
import { GameService } from 'src/app/shared/services/game.service';

@Component({
  selector: 'app-game-desk',
  templateUrl: './game-desk.component.html',
  styleUrls: ['./game-desk.component.css'],
  providers: [GameService]
})
export class GameDeskComponent implements OnInit {

  id: string;
  listRoundViewModel: GetGameGameView;
  done: boolean = false;

  score: Array<{ playerName: string, score: number }> = [];
  userScore: Array<{ playerName: string, score: number }> = [];

  requestTakeGameView: RequestTakeGameView;
  max: number = 0;
  constructor(private router: Router, private httpService: GameService, private activateRoute: ActivatedRoute) {

    this.id = activateRoute.snapshot.params['id'];
    this.requestTakeGameView = new RequestTakeGameView();
  }

  ngOnInit() {

    if (this.id) {
      this.loadGame();
    }
  }

  private loadGame() {

    this.httpService.getGame(this.id)
      .subscribe(
        response => {
          this.listRoundViewModel = response;
          response.rounds.forEach(element => this.max = element.number)

          response.rounds.forEach(element => {
            if (element.number == this.max) {
              this.countScore(element);
            }
          })
          this.done = true;
          this.listRoundViewModel.rounds.reverse();
        }
      )
  }

  public take() {

    this.requestTakeGameView.id = this.id;
    this.httpService.take(this.requestTakeGameView)
      .subscribe(
        response => {
          if (response.gameStatus == GameStatus.InProcess) {
            this.listRoundViewModel.rounds.unshift(response);
            this.countScore(response);
          }
          if (response.gameStatus == GameStatus.Finished) {
            this.enough();
          }
        }
      )
  }

  private countScore(element: any) {
    this.score = [];
    this.userScore = [];
    element.distributions.forEach(item => {
      if (item.isUser == true) {
        this.userScore.push({ playerName: item.name, score: item.score });
        return;
      }
      this.score.push({ playerName: item.name, score: item.score })
    })
  }

  public enough() {

    this.router.navigate(['/enough', this.id]);
  }
}