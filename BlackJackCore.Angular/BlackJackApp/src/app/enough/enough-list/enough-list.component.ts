import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RequestEnoughGameView } from 'src/app/shared/model/requestEnoughGameView';
import { ResponseEnoughGameView } from 'src/app/shared/model/responseEnoughGameView';
import { EnoughService } from 'src/app/shared/services/enough.service';

@Component({
    selector: 'app-enough-list',
    templateUrl: './enough-list.component.html',
    providers: [EnoughService]
})

export class EnoughListComponent implements OnInit {
    
    public id: string;
    public done: boolean = false;
    requestEnoughGame: RequestEnoughGameView;
    responseEnoughGame: ResponseEnoughGameView;

    constructor(private httpService: EnoughService, private activateRoure: ActivatedRoute){

        this.id = activateRoure.snapshot.params['id'];
        this.requestEnoughGame = new RequestEnoughGameView();
    }

    ngOnInit() {

        if (this.id) {
            this.loadEnough();
        }
    }

    private loadEnough() {

        this.requestEnoughGame.id = this.id;
        this.httpService.enough(this.requestEnoughGame)
        .subscribe(
            response => {
                this.responseEnoughGame = response;
                this.done = true;
            }
        )
    }

}