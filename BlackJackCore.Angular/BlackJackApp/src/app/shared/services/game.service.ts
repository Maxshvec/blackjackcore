import { Injectable } from "@angular/core";
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GetGameGameView, RoundGetGameGameViewItem } from 'src/app/shared/model/getGameGameView';
import { RequestTakeGameView } from 'src/app/shared/model/requestTakeGameView';


@Injectable({
    providedIn: 'root'
})

export class GameService {

    private baseUrl = environment.baseUrl;

    constructor(private http: HttpClient) { }

    public getGame(id: string): Observable<GetGameGameView> {

        return this.http.get<GetGameGameView>(this.baseUrl + 'Game/GetGame?id=' + id);
    }

    public take(model: RequestTakeGameView): Observable<RoundGetGameGameViewItem> {

        return this.http.post<RoundGetGameGameViewItem>(this.baseUrl + 'Game/Take', model);
    }
}
