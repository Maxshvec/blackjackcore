import { Injectable } from "@angular/core";
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GetHistoryGameView } from '../model/getHistoryGameView';


@Injectable({
    providedIn: 'root'
})

export class HistoryService {

    private baseUrl = environment.baseUrl;

    constructor(private http: HttpClient) { }

    public getHistory(): Observable<GetHistoryGameView> {

        return this.http.get<GetHistoryGameView>(this.baseUrl + 'Game/GetHistory');
    }
}
