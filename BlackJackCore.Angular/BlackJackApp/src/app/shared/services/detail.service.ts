import { Injectable } from "@angular/core";
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GetGameDetailsGameView } from 'src/app/shared/model/getGameDetailsGameView';


@Injectable({
    providedIn: 'root'
})

export class DetailService {

    private baseUrl = environment.baseUrl;

    constructor(private http: HttpClient) { }

    public getDetail(id: string): Observable<GetGameDetailsGameView> {

        return this.http.get<GetGameDetailsGameView>(this.baseUrl + 'Game/GetDetail?id=' + id);
    }
}