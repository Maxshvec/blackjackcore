import { Injectable } from "@angular/core";
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { RequestEnoughGameView } from 'src/app/shared/model/requestEnoughGameView';
import { ResponseEnoughGameView } from 'src/app/shared/model/responseEnoughGameView';


@Injectable({
    providedIn: 'root'
})

export class EnoughService {

    private baseUrl = environment.baseUrl;

    constructor(private http: HttpClient) { }

    public enough(model: RequestEnoughGameView): Observable<ResponseEnoughGameView> {

        return this.http.post<ResponseEnoughGameView>(this.baseUrl + 'Game/Enough', model);
    }

}
