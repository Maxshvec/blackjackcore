import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PlayerViewModel } from 'src/app/shared/model/playerViewModel';
import { RequestStartGameView } from 'src/app/shared/model/requestStartGameView';
import { RequestCreateUserGameView } from 'src/app/shared/model/requestCreateUserGameView';
import { environment } from 'src/environments/environment';
import { RequestTakeGameView } from 'src/app/shared/model/requestTakeGameView';
import { RequestEnoughGameView } from 'src/app/shared/model/requestEnoughGameView';
import { ResponseEnoughGameView } from 'src/app/shared/model/responseEnoughGameView';
import { GetGameGameView, RoundGetGameGameViewItem } from 'src/app/shared/model/getGameGameView';
import { GetHistoryGameView } from 'src/app/shared/model/getHistoryGameView';
import { GetGameDetailsGameView } from 'src/app/shared/model/getGameDetailsGameView';

@Injectable({
  providedIn: 'root'
})

export class StartService {

  private baseUrl = environment.baseUrl;

  constructor(private http: HttpClient) { }

  public createUser(model: RequestCreateUserGameView): Observable<RequestCreateUserGameView> {

    return this.http.post<RequestCreateUserGameView>(this.baseUrl + 'Game/CreateUser', model);
  }

  public getUsers(): Observable<PlayerViewModel[]> {

    return this.http.get<PlayerViewModel[]>(this.baseUrl + 'Game/GetUsers');
  }

  public startGame(model: RequestStartGameView): Observable<string> {
    
    return this.http.post<string>(this.baseUrl + 'Game/StartGame', model);
  }
}
