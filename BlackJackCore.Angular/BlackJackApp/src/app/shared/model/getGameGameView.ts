import { GameStatus } from './enum/gameStatus';

export class GetGameGameView {
    constructor(

        public rounds: Array<RoundGetGameGameViewItem>
    ) { }
}

export class RoundGetGameGameViewItem {

    public number: number;
    public gameStatus: GameStatus;

    constructor(

        public distributions: Array<DistributionGetGameGameViewItem>
    ) { }
}

export class DistributionGetGameGameViewItem {

    public playerId: string;
    public name: string;
    public isUser: boolean;
    public cardRank: string;
    public cardSuit: number;
    public suit: string;
    public score: number;
}
