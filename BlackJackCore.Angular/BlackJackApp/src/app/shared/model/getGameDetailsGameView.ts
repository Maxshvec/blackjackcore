export class GetGameDetailsGameView {

    public isOver:boolean;
    
    constructor(

        public rounds: Array<RoundGetGameDetailsViewItem>
    ) { }
}

export class RoundGetGameDetailsViewItem {

    public number: number;
    public gameHistoryId: string;

    constructor(

        public gamePlayers: Array<GamePlayerGetGameDetailsGameViewItem>,
        public winners: Array<WinnerGetGameDetailsGameViewItem>
    ) { }
}

export class GamePlayerGetGameDetailsGameViewItem {
    
        public playerId: string;
        public playerName: string;
        public cardRank: string;
        public cardSuit: string;
        public suit: string;
        public score: string;
        public status: number;
}

export class WinnerGetGameDetailsGameViewItem {

    public playerId: string;
    public playerName: string;
}