﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;

namespace BlackJackCore.Angular.Filters
{
    public class CustomExceptionFilterAttribute : Attribute, IExceptionFilter
    {
        public void OnException(ExceptionContext context)
        {
            string actionName = context.ActionDescriptor.DisplayName;
            string exceptionStack = context.Exception.StackTrace;
            string exceptionMessage = context.Exception.Message;


            context.Result =  new BadRequestObjectResult($"An exception occured in the {actionName} : \n { exceptionMessage } \n { exceptionStack }");  
             
 
            context.ExceptionHandled = true;
        }
    }
}
