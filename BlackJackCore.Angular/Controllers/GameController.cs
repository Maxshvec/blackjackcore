﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BlackJackCore.Angular.Filters;
using BlackJackCore.Logic.Interfaces;
using BlackJackCore.ViewModels.ViewModels.GameController;
using BlackJackCore.ViewModels.ViewModels.GameController.Request;
using BlackJackCore.ViewModels.ViewModels.GameController.Response;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace BlackJackCore.Angular.Controllers
{
    [CustomExceptionFilter]
    [Route("api/[controller]/[action]")]
    [EnableCors("AllowAllOrigin")]
    [ApiController]
    public class GameController : ControllerBase
    {
        IGameService _gameService;
        IPlayerManager _playerService;

        public GameController(IGameService gameService, IPlayerManager playerService)
        {
            _gameService = gameService;
            _playerService = playerService;
        }

        [HttpGet]
        public async Task<IActionResult> GetUsers()
        {
            List<PlayerGetUserGameViewItem> names = await _gameService.GetPlayers();

            return Ok(names);
        }

        [HttpPost]
        public async Task<IActionResult> StartGame([FromBody]RequestStartGameView request)
        {
            Guid gameId = await _gameService.StartGame(request.Name, request.NumberBots);
            
            return Ok(gameId);
        }

        [HttpPost]
        public async Task<IActionResult> CreateUser([FromBody]RequestCreateUserGameView user)
        {
            await _playerService.CreateUser(user.Name);
            
            return Ok(user);
        }

        [HttpGet]
        public async Task<IActionResult> GetGame(string id)
        {
            Guid gameId = new Guid(id);

            GetGameGameView listRound = new GetGameGameView();
            
            listRound.Rounds = await _gameService.GetGameCard(gameId);
            
            return Ok(listRound);
        }

        [HttpPost]
        public async Task<IActionResult> Take([FromBody]RequestTakeGameView request)
        {
            Guid gameId = new Guid(request.Id);

            ResponseTakeGameView currentRound = await _gameService.TakeDistribution(gameId);
            
            return Ok(currentRound);
        }

        [HttpPost]
        public async Task<IActionResult> Enough([FromBody]RequestEnoughGameView request)
        {
            Guid id = new Guid(request.Id);

            ResponseEnoughGameView response = new ResponseEnoughGameView();

            response.GamePlayers =  await _gameService.CompleteGame(id);
            
            return Ok(response);
        }

        [HttpGet]
        public async Task<IActionResult> GetHistory()
        {
            GetHistoryGameView listGameHistory = new GetHistoryGameView();

            listGameHistory.GameHistories = await _gameService.GetGameHistories();

            return Ok(listGameHistory);
        }

        [HttpGet]
        public async Task<IActionResult> GetDetail(string id)
        {
            GetGameDetailsGameView response = await _gameService.GetGameDetails(new Guid(id));
            
            return Ok(response);
        }
    }
}
