﻿using Microsoft.AspNetCore.Mvc;

namespace BlackJackCore.Angular.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
